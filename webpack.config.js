const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = (args = {}) => {
    const mode = args['mode'];
    
    const config = {
        entry: './src/app/index.js',
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'public')
        },
        module: {
            rules: [
                {
                    test: /\.vue?$/,
                    include: [
                        path.resolve(__dirname, "src/app")
                    ],
                    loader: 'vue-loader'
                },
                {
                    test: /\.css$/,
                    use: [
                        { loader: "style-loader" },
                        { loader: "css-loader" }
                    ]
                },
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                      loader: 'babel-loader',
                      options: {
                        presets: ['@babel/preset-env']
                      }
                    }
                },
                { 
                    test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                    loader: 'url-loader?limit=100000' 
                }
            ],
        },
        plugins: [
            new VueLoaderPlugin()
        ]
    }

    if (mode) {
        config['mode'] = mode;
    }

    return config;
};
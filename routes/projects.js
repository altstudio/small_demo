const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

const Project = require('../models/Project');

require('../config/passport')(passport);

router.put('/', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const _id = req.body.id;
        const project = await Project.findOne({ _id });
        if (!project) {
            throw 'Can not find project'
        }

        const fields = ['favorite', 'archive', 'commerce', 'manager'];
        for (field of fields) {
            if (req.body.hasOwnProperty(field)) {
                project[field] = req.body[field];
            }
        }

        await project.save();

        res.json({ success: true, project })
    } catch (e) {
        console.log('Project update error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

module.exports = router;
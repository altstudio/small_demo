const express = require('express');
const validator = require('express-validator');
const router = express.Router();
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const config = require('../config');
const passport = require('passport');

const User = require('../models/User');

require('../config/passport')(passport);

const hash = text => crypto.createHash('sha1').update(text).digest('base64');
const password = config.password;

router.use(validator());

router.get('/', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const users = await User.find();

        res.json({ success: true, users });
    } catch (e) {
        console.log('Get users error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

// change user
router.put('/user', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const id = req.body.id;
        const user = await User.findOne({ _id: id });
        if (!user) {
            throw { msg: 'This user is not exist' }
        }

        const fields = [
            'firstname',
            'lastname',
            'patronymic',
            'hubstaffId',
            'upworkUid',
            'card',
            'phone',
            'email',
            'birthday',
            'projectManager',
            'archive'
        ];

        for (field of fields) {
            if (req.body[field]) {
                user[field] = req.body[field];
            }
        }

        const links = ['vk', 'upwork'];
        user.links = [];
        for (link of links) {
            if (req.body[link]) {
                user.links.push({ name: link, value: req.body[link] });
            }
        }

        await user.save();

        res.json({ success: true });
    } catch (e) {
        console.log('Change user error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

// create user
router.post('/user', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const email = req.body.email;
        if (!email) {
            throw { msg: 'Email is require' };
        }

        const user = await User.findOne({ email });
        if (user) {
            throw { msg: 'This user is already exist' }
        }

        const newUser = {
            _id: new mongoose.Types.ObjectId(),
            email,
            lastConnect: null
        };

        const fields = [
            'firstname',
            'lastname',
            'patronymic',
            'hubstaffId',
            'upworkUid',
            'card',
            'phone',
            'birthday',
            'projectManager',
            'archive'
        ];
        
        for (field of fields) {
            if (req.body[field]) {
                newUser[field] = req.body[field];
            }
        }

        const links = ['vk', 'upwork'];
        newUser.links = [];
        for (link of links) {
            if (req.body[link]) {
                newUser.links.push({ name: link, value: req.body[link] });
            }
        }

        await new User(newUser).save();

        res.json({ success: true });
    } catch (e) {
        console.log('Save user error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

// get user
router.get('/user', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const id = req.query.id;
        if (!id) {
            throw { msg: 'ID is require' };
        }

        const user = await User.findOne({ _id: id });
        if (!user) {
            throw { msg: 'This user is not exist' }
        }

        res.json({ success: true, user });
    } catch (e) {
        console.log('Get user error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

router.get('/info', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        if (req.user.password !== hash(password)) throw { message: "Unauthorized" };

        return res.json(hash(password));
    } catch (e) {
        return res.status(500).json({success: false, error: e});
    }
});

router.post('/login', async (req, res) => {
    req.checkBody({
        password: {
            notEmpty: {
                errorMessage: "Это обязательное поле"
            }
        }
    });

    try {
        const errors = await req.getValidationResult();
        if (errors.array().length > 0) {
            return res.status(400).json({success: false, errors: errors.mapped(), msg: 'Bad request'});
        }

        if (req.body.password !== password) {
            // if user not found or password is not equial
            return res.status(400).send({ success: false, errors: { password: { msg: "Неверный пароль" } }, msg: 'Authentication failed' });
        }

        // payload for JWT
        const payload = {
            password: hash(password)
        };

        // generate JWT for 7 days
        const token = jwt.sign(payload, config.secret, { expiresIn: 60 * 60 * 24 * 7 });
        return res.json({ success: true, token: token });
    } catch (e) {
        console.log('Login error:', e);
        return res.status(500).json({success: false, error: e});
    }
});

// get last connect
router.get('/lastConnect', async (req, res) => {
    const source = req.body.source;
    const user = req.body.user;

    let currentUser = null;
    try {
        // get current user
        switch (source) {
            case 'upwork':
                currentUser = await User.findOne({ 'upworkUid': user });
                break;
    
            case 'hubstaff':
                currentUser = await User.findOne({ 'hubstaffId': user });
                break;
    
            default:
                throw { msg: 'Can not find source', value: source };
        }

        if (!currentUser) {
            throw { msg: 'Can not find user', value: user }
        }

        res.json({ success: true, lastConnect: currentUser.lastConnect });
    } catch (e) {
        console.log('Report save error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

module.exports = router;
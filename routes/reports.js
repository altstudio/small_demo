const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

const User = require('../models/User');
const Report = require('../models/Report');
const Project = require('../models/Project');

require('../config/passport')(passport);

const getMonday = (d) => {
    d = new Date(d);
    const day = d.getDay();
    const diff = d.getDate() - day + (day == 0 ? -6:1);
    return new Date(d.setDate(diff)).getTime();
}

const getDay = (d) => Number.parseInt(new Date(d).getTime() / (1000 * 3600 * 24)) * 3600 * 24 * 1000;

// add reports
router.post('/', async (req, res) => {
    const source = req.body.source;
    const user = req.body.user;

    let currentUser = null;
    try {
        // get current user
        switch (source) {
            case 'upwork':
                currentUser = await User.findOne({ 'upworkUid': user });
                break;
    
            case 'hubstaff':
                currentUser = await User.findOne({ 'hubstaffId': user });
                break;
    
            default:
                throw { msg: 'Can not find source', value: source };
        }

        if (!currentUser) {
            throw { msg: 'Can not find user', value: user }
        }

        // add reports
        if (!req.body.reports || (req.body.reports && !req.body.reports.length)) {
            throw { msg: 'Can not find reports', user }
        }
        
        const reports = [];
        for (let report of req.body.reports) {
            // check fields of report
            if (!report.date || !report.time || !report.project) {
                throw { msg: 'Report fields are empty', report }
            }

            let project = await Project.findOne({ name: report.project });
            if (!project) {
                project = await new Project({
                    _id: new mongoose.Types.ObjectId(),
                    name: report.project
                }).save();
            }

            const dbReport = await Report.findOne({ 
                user: currentUser._id,
                date: report.date,
                project: project._id
            });

            if (!dbReport) {
                // need to add new report
                reports.push(new Report({
                    _id: new mongoose.Types.ObjectId(),
                    source,
                    user: currentUser._id,
                    date: report.date,
                    time: report.time,
                    project: project._id
                }).save());
            } else if (dbReport.time != report.time) {
                dbReport.time = report.time;
                reports.push(dbReport.save());
            }
        }

        await Promise.all(reports);
        res.json({ success: true });

        currentUser.lastConnect = Date.now();
        currentUser.save();
    } catch (e) {
        console.log('Report save error: ', e);
        res.json({ success: false, error: e });
    }
});

router.post('/edit', async (req, res) => {
    const source = req.body.source;
    const user = req.body.user;

    let currentUser = null;
    try {
        // get current user
        switch (source) {
            case 'upwork':
                currentUser = await User.findOne({ 'upworkUid': user });
                break;
    
            case 'hubstaff':
                currentUser = await User.findOne({ 'hubstaffId': user });
                break;
    
            default:
                throw { msg: 'Can not find source', value: source };
        }

        if (!currentUser) {
            throw { msg: 'Can not find user', value: user }
        }

        // add reports
        if (!req.body.reports || (req.body.reports && !req.body.reports.length)) {
            throw { msg: 'Can not find reports', user }
        }

        const reports = [];
        // сначала летят изменения, потом летит список репортов
        // по изменениям смотрим на проект. Если есть репорт за этот день по этому проекту - сносим
        // в случае, если репорты все же есть, они восстановятся в списке репортов
        for (let report of req.body.reports) {
            // check action
            if (report.action !== 'edit' && report.action !== 'delete') {
                continue;
            }

            const project = await Project.findOne({ name: report.action === 'edit' ? report.project_from : report.project_to });
            if (!project) {
                continue;
            }

            // ищем репорт по проекту за нужный день
            const dbReport = await Report.findOne({
                user: currentUser._id,
                date: report.date,
                project: project._id
            })

            if (!dbReport) {
                continue;
            }

            await dbReport.remove();
        }

        res.json({ success: true });
    } catch (e) {
        console.log('Report edit error: ', e);
        res.json({ success: false, error: e });
    }
});

// todo: change request to db
router.get('/simple', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const start = +req.query.start;
        const commerce = req.query.hasOwnProperty('commerce') ? (req.query.commerce === 'true') : false;

        const currentDay = Number.parseInt(new Date().getTime() / (1000 * 3600 * 24)) * 3600 * 24 * 1000;
        const monday = start || getMonday(currentDay);
        const lastMonday = monday - (1000 * 3600 * 24 * 7);

        // get reports just from 3 last weeks for fast request
        const mainPipline = [
            /*{ $match: { $and: [{ date: { $gte: lastMonday - (1000 * 3600 * 24 * 7) } }, { date: { $lte: monday + (1000 * 3600 * 24 * 7) } }] } }*/
        ];

        mainPipline.push({
            $lookup: {
                from: 'projects',
                localField: 'project',
                foreignField: '_id',
                as: 'project'
            }
        });
        mainPipline.push({ $unwind: '$project' });

        if (commerce) {
            mainPipline.push({ $match: { 'project.commerce': true } });
        }

        mainPipline.push(...[
            { 
                $group: { 
                    '_id': '$user',
                    'all': { $sum: '$time' },
                    'one': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", monday ] }, { $lt: [ "$date", monday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'last': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday ] }, { $lt: [ "$date", lastMonday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'lastTwo': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday - (1000 * 3600 * 24 * 7) ] }, { $lte: [ "$date", monday - (1000 * 3600 * 24) ] }] }, then: '$time', else: 0 } } },
                }
            }, {
                $lookup: {
                    from: 'users',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'user'
                }
            }, {
                $unwind: '$user'
            }
        ]);

        // need to get all reports by users
        const reports = await Report.aggregate([
            ...mainPipline
        ]);

        reports.forEach(report => {
            report.user.online = !!req.app.io.extensionClients[report.user._id];
        })

        res.json({ success: true, reports })
    } catch (e) {
        console.log('Report simple get error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

router.post('/update', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const user = await User.findById(req.body.user);
        const socketClient = req.app.io.extensionClients[user._id];
        if (socketClient) {
            socketClient.emit('updateReports');
        }
        res.json({ success: true })
    } catch (err) {
        console.log(err);
        return res.json({ success: false })
    }
})

router.get('/user/:id', passport.authenticate('jwt', {session: false}), async (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.status(500).json({ success: false, error: 'Can not find user' });
    }

    try {
        const start = +req.query.start;
        const currentDay = Number.parseInt(new Date().getTime() / (1000 * 3600 * 24)) * 3600 * 24 * 1000;
        const monday = start || getMonday(currentDay);
        const lastMonday = monday - (1000 * 3600 * 24 * 7);

        const user = await User.findOne({ _id: id });

        if (!user) {
            res.status(500).json({ success: false, error: 'Can not find user' });
        }

        const mainPipline = [
            {
                $match: { user: user._id }
            },
            {
                $group: { 
                    '_id': '$project',
                    'all': { $sum: '$time' },
                    'one': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", monday ] }, { $lt: [ "$date", monday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'last': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday ] }, { $lt: [ "$date", lastMonday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'lastTwo': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday - (1000 * 3600 * 24 * 7) ] }, { $lte: [ "$date", monday - (1000 * 3600 * 24) ] }] }, then: '$time', else: 0 } } },
                }
            }, {
                $lookup: {
                    from: 'projects',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'project'
                }
            }, {
                $unwind: '$project'
            }
        ]

        // need to get all reports by users
        const reports = await Report.aggregate([
            ...mainPipline
        ]);

        res.json({ success: true, reports, user })
    } catch (e) {
        console.log('Report simple get error: ', e);
        res.status(500).json({ success: false, error: e });
    }
})

router.get('/projects', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const start = +req.query.start;
        const currentDay = Number.parseInt(new Date().getTime() / (1000 * 3600 * 24)) * 3600 * 24 * 1000;
        const monday = start || getMonday(currentDay);
        const lastMonday = monday - (1000 * 3600 * 24 * 7);

        const mainPipline = [
            { 
                $group: { 
                    '_id': '$project',
                    'all': { $sum: '$time' },
                    'one': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", monday ] }, { $lt: [ "$date", monday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'last': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday ] }, { $lt: [ "$date", lastMonday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'lastTwo': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday - (1000 * 3600 * 24 * 7) ] }, { $lte: [ "$date", monday - (1000 * 3600 * 24) ] }] }, then: '$time', else: 0 } } },
                }
            }, {
                $lookup: {
                    from: 'projects',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'project'
                }
            }, {
                $unwind: '$project'
            }
        ]

        // need to get all reports by users
        const reports = await Report.aggregate([
            ...mainPipline
        ]);

        res.json({ success: true, reports })
    } catch (e) {
        console.log('Report simple get error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

router.get('/projects/:id', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const start = +req.query.start;
        const currentDay = Number.parseInt(new Date().getTime() / (1000 * 3600 * 24)) * 3600 * 24 * 1000;
        const monday = start || getMonday(currentDay);
        const lastMonday = monday - (1000 * 3600 * 24 * 7);

        const mainPipline = [
            {
                $match: { project: new mongoose.Types.ObjectId(req.params.id) }
            },
            { 
                $group: { 
                    '_id': '$user',
                    'all': { $sum: '$time' },
                    'one': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", monday ] }, { $lt: [ "$date", monday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'last': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday ] }, { $lt: [ "$date", lastMonday + (1000 * 3600 * 24 * 7) ] }] }, then: '$time', else: 0 } } },
                    'lastTwo': { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", lastMonday - (1000 * 3600 * 24 * 7) ] }, { $lte: [ "$date", monday - (1000 * 3600 * 24) ] }] }, then: '$time', else: 0 } } },
                }
            }, {
                $lookup: {
                    from: 'users',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'user'
                }
            }, {
                $unwind: '$user'
            }
        ]

        // need to get all reports by users
        const reports = await Report.aggregate([
            ...mainPipline
        ]);

        const project = await Project.findOne({ _id: req.params.id }).populate('manager');

        res.json({ success: true, project, reports })
    } catch (e) {
        console.log('Report simple get error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

router.get('/detailed', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const start = req.query.start;
        const end = req.query.end;
        const commerce = req.query.hasOwnProperty('commerce') ? (req.query.commerce === 'true') : false;

        let users = await User.find();
        users = users.map(u => {
            u = Object.assign({ reports: [] }, u._doc);
            return u;
        })

        if (!start || !end) {
            return res.json({ success: true, users })
        }

        const $group = { '_id': '$user' };
        const temp = { total: 0, upwork: 0, hubstaff: 0 };

        for (let t = +start; t <= +end; t+= 1000 * 3600 * 24) {
            temp[t] = 0;
            $group[t] = {
                $sum: { $cond: { if: { $and: [ { $gte: [ "$date", t ] }, { $lt: [ "$date", t + (1000 * 3600 * 24) ] }] }, then: '$time', else: 0 } }
            }
        }

        $group.total = { $sum: '$time' };
        $group.upwork = { $sum: { $cond: { if: { $eq: ['$source', 'upwork'] }, then: '$time', else: 0 } } };
        $group.hubstaff = { $sum: { $cond: { if: { $eq: ['$source', 'hubstaff'] }, then: '$time', else: 0 } } };
        $group.price = { $sum: { $multiply: [ { $divide: [ '$time', 3600 ] }, '$price' ] } };

        const mainPipline = [];
        mainPipline.push({
            $match: {
                $and: [
                    { date: { $gte: +start } },
                    { date: { $lte: +end } }
                ] 
            }
        });
        mainPipline.push({
            $lookup: {
                from: 'projects',
                localField: 'project',
                foreignField: '_id',
                as: 'project'
            }
        });
        mainPipline.push({ $unwind: '$project' });

        if (commerce) {
            mainPipline.push({ $match: { 'project.commerce': true } });
        }

        mainPipline.push({ $group });
        
        // need to get all reports by users
        const reports = await Report.aggregate([
            ...mainPipline
        ]);

        users = users.map(u => {
            u.reports = reports.find(r => r && u && r._id && u._id ? r._id.toString() === u._id.toString() : false) || Object.assign({}, temp);
            if (u.reports._id){
                delete u.reports._id;
            }
            return u;
        })

        res.json({ success: true, users })
    } catch (e) {
        console.log('Report detailed get error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

router.get('/detailed/:id', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const currentDay = Number.parseInt(new Date().getTime() / (1000 * 3600 * 24)) * 3600 * 24 * 1000;
        const monday = getMonday(currentDay);

        const start = req.query.start || monday;
        const end = req.query.end || (monday + 1000 * 3600 * 24 * 6);
        const userId = req.params.id;

        const user = await User.findOne({ _id: userId });

        const mainPipline = [
            { $match: { user: new mongoose.Types.ObjectId(user._id) }},
            { $match: { $and: [{ date: { $gte: +start } }, { date: { $lte: +end } }] } },
            {
                $lookup: {
                    from: 'projects',
                    localField: 'project',
                    foreignField: '_id',
                    as: 'project'
                }
            }, {
                $unwind: '$project'
            },
            {
                $group: {
                    _id: '$date',
                    day: { $push: { _id: '$_id', 'project': '$project', 'time': '$time', price: '$price' } },
                    totalTime: { $sum: '$time' },
                    totalPrice: { $sum: { $divide: [ { $multiply : [ '$time', '$price' ] }, 3600 ] } }
                }
            }
        ]

        // need to get all reports by users
        let reports = await Report.aggregate([
            ...mainPipline
        ]);

        for (let t = +start; t <= +end; t+= 1000 * 3600 * 24) {
            const d = reports.find(r => r._id === t);
            if (!d) {
                reports.push({
                    _id: +t,
                    totalTime: 0,
                    day: []
                })
            }
        }

        res.json({ success: true, user, reports })
    } catch (e) {
        console.log('Report detailed user get error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

router.put('/price', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const reports = req.body.reports;
        const price = req.body.price;

        await Promise.all(reports.map(r => {
            return Report.update({ _id: r }, { $set: { price } })
        }));

        res.json({ success: true })
    } catch (e) {
        console.log('Report detailed user get error: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

router.get('/chart', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const start = +req.query.start;
        const count = +req.query.count || 5;
        const currentDay = Number.parseInt(new Date().getTime() / (1000 * 3600 * 24)) * 3600 * 24 * 1000;
        const monday = getMonday(currentDay);

        const s = monday - count * 7 * 3600 * 24 * 1000;
        const e = start;

        const mainPipline = [];

        const projects = await Project.aggregate([
            { $group: { _id: '$commerce', projects: { $push: '$_id' } } }
        ]);

        console.log(projects);

        mainPipline.push({
            $match: {
                $and: [
                    { date: { $gte: +s } },
                    { date: { $lte: +e } }
                ] 
            },
        });

        mainPipline.push({
            $group: {
                _id: null,
                all: {
                    $push: { date: '$date', time: '$time' }
                },
                commerce: {
                    $push: {
                        $cond: [
                            { $in: ["$project", projects.find(p => p._id === true).projects.map(p => mongoose.Types.ObjectId(p)) ] },
                            { date: '$date', time: '$time' },
                            null
                        ]
                    }
                },
                nocommerce: {
                    $push: {
                        $cond: [
                            { $in: ["$project", projects.find(p => p._id === false).projects.map(p => mongoose.Types.ObjectId(p)) ] },
                            { date: '$date', time: '$time' },
                            null
                        ]
                    }
                }
            }
        });

        mainPipline.push({
            $project: {
                all: { $filter: { input: '$all', as: 'item', cond: { $not: [{$eq: ['$$item', null] }] } } },
                commerce: { $filter: { input: '$commerce', as: 'item', cond: { $not: [{$eq: ['$$item', null] }] } } },
                nocommerce: { $filter: { input: '$nocommerce', as: 'item', cond: { $not: [{$eq: ['$$item', null] }] } } },
            }
        });

        const group = { _id: null, _commerce: { $push: {} }, _nocommerce: { $push: {} }, _all: { $push: {} } };
        const project = { _id: 0 };
        
        for (let i = 1; i < count + 1; i++) {
            const sWeek = i * 7 * 3600 * 24 * 1000;
            const nWeek = (i + 1) * 7 * 3600 * 24 * 1000;

            group['_all']['$push'][s + sWeek] = { $sum: { $cond: { if: { $and: [ { $gte: [ "$all.date", s + sWeek ] }, { $lt: [ "$all.date", s + nWeek ] }] }, then: '$all.time', else: 0 } } }
            group['_commerce']['$push'][s + sWeek] = { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", s + sWeek ] }, { $lt: [ "$date", s + nWeek ] }] }, then: '$time', else: 0 } } }
            group['_nocommerce']['$push'][s + sWeek] = { $sum: { $cond: { if: { $and: [ { $gte: [ "$date", s + sWeek ] }, { $lt: [ "$date", s + nWeek ] }] }, then: '$time', else: 0 } } }
        }

        mainPipline.push({ $group: group });
        mainPipline.push({ $project: project });

        console.log(JSON.stringify(mainPipline));

        const data = await Report.aggregate([
            ...mainPipline
        ]);

        res.json({ success: true, data: data[0] })
    } catch (e) {
        console.log('Report charts get: ', e);
        res.status(500).json({ success: false, error: e });
    }
});

module.exports = router;
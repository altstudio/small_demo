const gulp = require("gulp");
const webpack = require("webpack");
const loadPlugins = require("gulp-load-plugins");

const vueWebpackConfig = require("./webpack.config");

const plugins = loadPlugins();

gulp.task('bundle', (cb) => {
    // need to build Vue application
    webpack(vueWebpackConfig({ mode: 'development' }), (err, stats) => {
        if (err) console.log(err);

        console.log(stats.toString());

        cb();
    });
});

gulp.task('bundle-build', (cb) => {
    // need to build Vue application
    webpack(vueWebpackConfig({ mode: 'production' }), (err, stats) => {
        if (err) console.log(err);

        console.log(stats.toString());

        cb();
    });
});

gulp.task('default', ['bundle']);

gulp.task('build', ['bundle-build']);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Project = new Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    favorite: {
        type: Boolean,
        default: false
    },
    archive: {
        type: Boolean,
        default: false
    },
    commerce: {
        type: Boolean,
        default: true
    },
    manager: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Project', Project);
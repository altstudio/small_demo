const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
    _id: Schema.Types.ObjectId,
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    patronymic: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    card: {
        type: String,
    },
    phone: {
        type: String
    },
    birthday: {
        type: Number
    },
    hubstaffId: {
        type: String
    },
    upworkUid: {
        type: String
    },
    projectManager: {
        type: Boolean,
        default: false,
    },
    archive: {
        type: Boolean,
        default: false
    },
    links: [
        {
            name: String,
            value: String
        }
    ],
    lastConnect: {
        type: String
    }
});

module.exports = mongoose.model('User', User);
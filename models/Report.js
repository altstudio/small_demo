const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Report = new Schema({
    _id: Schema.Types.ObjectId,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    source: {
        type: String
    },
    date: {
        type: Number
    },
    time: {
        type: Number
    },
    project: {
        type: Schema.Types.ObjectId,
        ref: 'Project'
    },
    price: {
        type: Number
    }
});

module.exports = mongoose.model('Report', Report);
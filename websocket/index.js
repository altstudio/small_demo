const config = require('../config');
const socketioJwt = require('socketio-jwt');

const User = require('../models/User');

module.exports = (server, app) => {
    const io = require('socket.io')(server, {
        pingTimeout: -1
    });
    const clients = [];
    
    const extensionIo = io.of('/extension');
    const extensionClients = {};

    app.io = io;
    io.clients = clients;
    io.extensionClients = extensionClients;

    extensionIo.on('connection', socket => {
        socket.on('authorize', async ({ hubstaff_id, upwork_id }) => {
            if (!hubstaff_id && !upwork_id) {
                return null;
            }

            const user = await User.findOne({
                ...(hubstaff_id && { hubstaffId: hubstaff_id }),
                ...(upwork_id && { upworkUid: upwork_id})
            });
            if (user) {
                // Для одного юзера должен быть 1 сокет
                socket.user = user._id;
                if (!extensionClients[user._id]) {
                    console.log(`${user.firstname} ${user.lastname} connected from extension!`);
                    for (let client of clients) {
                        client.emit('extConnect', { user: { ...user._doc, connectedAt: socket.connectedAt } });
                    }
                }
                extensionClients[user._id] = socket;
            }
        });

        socket.on('disconnect', async (reason) => {
            try {
                const userId = socket.user;
                if (userId) {
                    extensionClients[userId].disconnected = true;
    
                    // Подождем пару секунд. Вдруг подключится
                    await new Promise(resolve => setTimeout(resolve, 2000));
                    if (extensionClients[userId] && !extensionClients[userId].disconnected) {
                        return;
                    }

                    delete extensionClients[userId];
                    for (let client of clients) {
                        client.emit('extDisconnect', { user: userId });
                    }
                }
            } catch (err) {
                console.log(err);
            }
        })
    })
    
    io.on('connection', socketioJwt.authorize({
        secret: config.secret
    })).on('authenticated', async client => {
        client.emit('authorized');

        clients.push(client);

        let users = [];
        // send all connected from extension users
        for (let _id in extensionClients) {
            const user = await User.findById(_id);
            users.push(user._doc)
        }

        client.emit('online', { users })
    })
}
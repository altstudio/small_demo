const app = require('./app');
const http = require('http');

const port = '3000';
app.set('port', port); //port to backend server

const server = http.createServer(app);

server.listen(port);

const io = require('./websocket')(server, app);
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

// load up the user model
const config = require('./index'); // get db config file
const crypto = require('crypto');

const hash = text => crypto.createHash('sha1').update(text).digest('base64');
const password = config.password;

module.exports = function(passport) {
    const opts = {};
    opts.jwtFromRequest = ExtractJwt.fromExtractors(
        [
            ExtractJwt.fromAuthHeaderAsBearerToken()
        ]
    );
    opts.secretOrKey = config.secret;
    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
        if (hash(password) === jwt_payload.password) {
            return done(null, { password: hash(password) });
        }
        done(null, false);
    }));
};
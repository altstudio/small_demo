import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

import App from "./App.vue";
import Workers from "./routes/workers/Workers.vue"
import Main from "./routes/Main.vue"
import Projects from "./routes/projects/Projects.vue"
import Project from "./routes/projects/Project.vue"
import Worker from "./routes/workers/Worker.vue"

import Login from "./routes/Login.vue";

import WorkerDetailed from "./routes/workers/PersonDetailed.vue"
import WorkersList from "./routes/workers/List.vue";
import WorkserCreate from "./routes/workers/Create.vue";
import WorkersDetailed from "./routes/workers/Detailed.vue";

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/main',
            meta: {
                auth: true
            }
        },
        {
            path: '/login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/main',
            name: 'index',
            component: Main,
            meta: {
                auth: true
            }
        }, {
            path: '/workers',
            name: 'workers-list',
            meta: {
                auth: true
            },
            component: WorkersList
        }, {
            path: '/workers/create',
            name: 'worker-create',
            meta: {
                auth: true
            },
            component: WorkserCreate
        }, {
            path: '/workers/edit/:id',
            name: 'worker-edit',
            props: true,
            meta: {
                auth: true
            },
            component: WorkserCreate
        }, {
            path: '/workers/stat/simple',
            name: 'workers',
            meta: {
                auth: true
            },
            component: Workers
        }, {
            path: '/workers/stat/detailed',
            name: 'workers-detailed',
            meta: {
                auth: true
            },
            component: WorkersDetailed
        }, {
            path: '/workers/:id/detailed',
            name: 'detailed',
            props: true,
            meta: {
                auth: true
            },
            component: WorkerDetailed
        }, {
            path: '/projects/stat/simple',
            name: 'projects',
            meta: {
                auth: true
            },
            component: Projects
        }, {
            path: '/projects/:project',
            name: 'project',
            meta: {
                auth: true
            },
            props: true,
            component: Project
        }, {
            path: '/workers/:id/profile',
            name: 'worker',
            meta: {
                auth: true
            },
            props: true,
            component: Worker
        }
    ]
});
import 'vuetify/dist/vuetify.min.css'

import Vuetify from 'vuetify'
import Vue from 'vue';
import App from './App.vue';
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueApexCharts from 'vue-apexcharts'
import Vuex from 'vuex';
import VueSocketIO from 'vue-socket.io'
import VuexStore from './vuex';

import config from './config/config';

Vue.use(Vuex);

Vue.use(VueApexCharts);
Vue.use(Vuetify);
Vue.use(VueAxios, axios);

Vue.axios.defaults.baseURL = config.baseURL;

Vue.router = router;

Vue.use(require('@websanova/vue-auth'), {
    auth: require('./config/auth.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    // rolesVar: 'type', // variable for type
    authRedirect: {path: '/login'},
    forbiddenRedirect: {path: '/403'},
    notFoundRedirect: {path: '/404'},
    registerData: {url: 'api/users/register', method: 'POST', redirect: '/users', fetchUser: false},
    loginData: {url: 'api/users/login', method: 'POST', redirect: '/', fetchUser: true},
    logoutData: {url: 'api/users/logout', method: 'POST', redirect: '/login', makeRequest: false},
    fetchData: {url: 'api/users/info', method: 'GET', enabled: true},
    refreshData: {url: 'api/users/refresh', method: 'GET', enabled: false, interval: 30},
    parseUserData: function (data) {
        return data;
    }
});

Vue.use(new VueSocketIO({
    debug: true,
    connection: `${config.socket}`
}))

const store = new Vuex.Store(VuexStore);

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
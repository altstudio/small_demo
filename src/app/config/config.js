const configs = {
    development: {
        baseURL: 'http://localhost:3000/',
        socket: 'localhost:3000'
    }
}

export default configs[process.env.NODE_ENV || 'development'];
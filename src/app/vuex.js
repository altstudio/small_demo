export default {
    state: {
        online: []
    },
    mutations: {
        connect(state, user) {
            state.online.push(user);
        },
        disconnect(state, user) {
            state.online = state.online.filter(u => u._id !== user)
        },
        initOnline(state, users) {
            state.online = users;
        }
    }
}
export default {
    hoursFormat: (time) => {
        if (!time) return '0:00:00';
        const hours = Number.parseInt(time / 3600);
        const minutes = Number.parseInt((time - hours * 3600) / 60);
        const seconds = Number.parseInt(time -  hours * 3600 - minutes * 60);

        const minutesStr = minutes.toString().length === 1 ? `0${minutes}` : minutes;
        const secondsStr = seconds.toString().length === 1 ? `0${seconds}` : seconds;
        
        return `${hours}:${minutesStr}:${secondsStr}`;
    }
}
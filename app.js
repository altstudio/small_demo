const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const swStats = require('swagger-stats');

const config = require('./config');

const app = express(); //init express

mongoose.Promise = global.Promise; // connect to database
mongoose.connect(config.server.database, {
    useMongoClient: true
});

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname + '/node_modules'));

app.use(require('cors')());

app.use(swStats.getMiddleware({ 
    name: 'Alt-stats'
}));

app.use('/api', require('./routes/main'));
app.use('/api/users', require('./routes/users'));
app.use('/api/reports', require('./routes/reports'));
app.use('/api/projects', require('./routes/projects'));

app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = err;

    res.status(err.status || 500);
    res.json({msg: err.message});
});

module.exports = app;